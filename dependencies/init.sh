#!/bin/bash
#### THIS IS COMMON INITIALIZATION SCRIPT

. /usr/local/openiam/env.conf
export VAULT_CERTS="$HOME_DIR/vault/certs/"
export JAVA_HOME="$HOME_DIR/jdk"

#common
rpm -i --nosignature $HOME_DIR/dependencies/oniguruma-5.9.5-3.el7.x86_64.rpm
rpm -i --nosignature $HOME_DIR/dependencies/jq-1.5-1.el7.x86_64.rpm
rpm -i --nosignature $HOME_DIR/dependencies/tcp_wrappers-7.6-77.el7.x86_64.rpm
rpm -i --nosignature $HOME_DIR/dependencies/socat-1.7.3.2-1.1.el7.x86_64.rpm
rpm -i --nosignature $HOME_DIR/dependencies/logrotate-3.8.6-19.el7.x86_64.rpm
rpm -i --nosignature $HOME_DIR/dependencies/m4-1.4.16-10.el7.x86_64.rpm

set -e

if [ -f "$HOME_DIR/initialized" ]; then
  echo "OpenIAM is already initialized. Stop initialization. to reinstall use rpm -e to remove openiam and reinstall rpm again"
  exit 1
fi

#init vault.
$HOME_DIR/utils/vault/init.sh
$HOME_DIR/utils/rabbitmq/init.sh
$HOME_DIR/utils/elasticsearch/init.sh
$HOME_DIR/utils/redis/init.sh
#flyway
chmod +x $HOME_DIR/utils/flyway/init.sh
$HOME_DIR/utils/flyway/init.sh

proxy_rpms=$(find /usr/local/openiam/dependencies/ -name "mod_openiam*" | sort -r)
latest_proxy_rpm=$(echo $proxy_rpms | cut -d ' ' -f1)

if [ -n "$latest_proxy_rpm" ]; then
	rpm -U "$latest_proxy_rpm"
fi

systemctl start httpd
systemctl enable --now httpd

echo "1" >/usr/local/openiam/initialized

systemctl start openiam-auth.service
systemctl start openiam-device.service
systemctl start openiam-email.service
systemctl start openiam-esb.service
systemctl start openiam-groovy.service
systemctl start openiam-idm.service
systemctl start openiam-reconciliation.service
systemctl start openiam-synchronization.service
systemctl start openiam-ui.service
systemctl start openiam-workflow.service

systemctl enable openiam-auth.service
systemctl enable openiam-device.service
systemctl enable openiam-email.service
systemctl enable openiam-esb.service
systemctl enable openiam-groovy.service
systemctl enable openiam-idm.service
systemctl enable openiam-reconciliation.service
systemctl enable openiam-synchronization.service
systemctl enable openiam-ui.service
systemctl enable openiam-workflow.service

openiam-cli status
