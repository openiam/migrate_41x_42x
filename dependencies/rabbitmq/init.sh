#!/bin/bash

set -e
#### THIS IS RABBITMQ INITIALIZATION SCRIPT
. /usr/local/openiam/env.conf
export VAULT_CERTS="$HOME_DIR/vault/certs/"
export JAVA_HOME="$HOME_DIR/jdk"
export VAULT_HOME="$HOME_DIR/utils/vault/"


. ${VAULT_HOME}validate.vault.sh


export RABBITMQ_PASSWORD=$(. ${VAULT_HOME}vault.fetch.property.sh vault.secret.rabbitmq.password)

if [ -z "$RABBITMQ_PASSWORD" ] || [ "$RABBITMQ_PASSWORD" == "null" ]; then
	echo "cannot get vault.secret.rabbitmq.password property from vault"
	exit 1;
fi

#rabbitmq
rpm -U --nosignature $HOME_DIR/dependencies/erlang-22.3.4.2-1.el7.x86_64.rpm
rpm -U --nosignature $HOME_DIR/dependencies/rabbitmq-server-3.8.5-1.el7.noarch.rpm
rm -rf /var/lib/rabbitmq/mnesia/*

cp $HOME_DIR/dependencies/rabbitmq_delayed_message_exchange-3.8.0.ez /usr/lib/rabbitmq/lib/rabbitmq_server-3.8.5/plugins/
systemctl enable rabbitmq-server
systemctl start rabbitmq-server
echo "Starting RabbitMQ..."
sleep 5

rabbitmq-plugins enable rabbitmq_delayed_message_exchange
rabbitmq-plugins enable rabbitmq_management


rabbitmqctl add_vhost openiam_am
rabbitmqctl add_vhost openiam_idm
rabbitmqctl add_vhost openiam_audit
rabbitmqctl add_vhost openiam_common
rabbitmqctl add_vhost openiam_connector
rabbitmqctl add_vhost openiam_activiti
rabbitmqctl add_vhost openiam_user
rabbitmqctl add_vhost openiam_groovy_manager
rabbitmqctl add_vhost openiam_synchronization
rabbitmqctl add_vhost openiam_ext_log
rabbitmqctl add_vhost openiam_bulk_synchronization
rabbitmqctl add_vhost openiam_reconciliation
rabbitmqctl add_vhost openiam_bulk_reconciliation

rabbitmqctl add_user openiam $RABBITMQ_PASSWORD
rabbitmqctl set_user_tags openiam administrator

rabbitmqctl set_permissions -p openiam_am openiam ".*" ".*" ".*"
rabbitmqctl set_permissions -p openiam_idm openiam ".*" ".*" ".*"
rabbitmqctl set_permissions -p openiam_audit openiam ".*" ".*" ".*"
rabbitmqctl set_permissions -p openiam_common openiam ".*" ".*" ".*"
rabbitmqctl set_permissions -p openiam_connector openiam ".*" ".*" ".*"
rabbitmqctl set_permissions -p openiam_activiti openiam ".*" ".*" ".*"
rabbitmqctl set_permissions -p openiam_user openiam ".*" ".*" ".*"
rabbitmqctl set_permissions -p openiam_groovy_manager openiam ".*" ".*" ".*"
rabbitmqctl set_permissions -p openiam_synchronization openiam ".*" ".*" ".*"
rabbitmqctl set_permissions -p openiam_ext_log openiam ".*" ".*" ".*"
rabbitmqctl set_permissions -p openiam_bulk_synchronization openiam ".*" ".*" ".*"
rabbitmqctl set_permissions -p openiam_reconciliation openiam ".*" ".*" ".*"
rabbitmqctl set_permissions -p openiam_bulk_reconciliation openiam ".*" ".*" ".*"
