#!/bin/bash

set -e


. /usr/local/openiam/env.conf
export VAULT_CERTS="$HOME_DIR/vault/certs/"
. $HOME_DIR/utils/vault/validate.vault.sh

function random() {
	echo $(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
}

# $1 name of var, $2 is default val, $3 is message to show.
function input() {
        echo -e "$3, default: $2"
        read -s value
        if [ -z "$value" ]; then
                value=$2
        fi
	export $1=$value
}
echo -e "Generate OpenIAM encryption secrets"
#Users should not be avare of it. Also we can use vault API to retrieve this info if required.
export SECRET_IAM_JKS_PASSWORD=$(cat /usr/local/OpenIAM/data/openiam/conf/securityconf.properties | grep iam.jks.password | cut -d '=' -f2)
export SECRET_IAM_JKS_KEY_PASSWORD=$(cat /usr/local/OpenIAM/data/openiam/conf/securityconf.properties | grep iam.jks.key.password | cut -d '=' -f2)
export SECRET_IAM_JKS_COOKIE_KEY_PASSWORD=$(cat /usr/local/OpenIAM/data/openiam/conf/securityconf.properties | grep iam.jks.cookie.key.password | cut -d '=' -f2)
export SECRET_IAM_JKS_COMMON_KEY_PASSWORD=$(cat /usr/local/OpenIAM/data/openiam/conf/securityconf.properties | grep iam.jks.common.key.password | cut -d '=' -f2)
export SECRET_KEYSTORE_PASSWORD="changeit"

echo -e "=============== CRITICAL SECTION ==============="
echo -e "Database"
input "SECRET_JDBC_USERNAME" "idmuser" "Set OpenIAM username for schema 'openiam' "
input "SECRET_JDBC_PASSWORD" "idmuser" "Set OpenIAM password for schema 'openiam' "

input "SECRET_ACTIVITI_JDBC_USERNAME" "idmuser" "Set OpenIAM username for schema 'activiti'. For MySQL it will be the same as for 'openiam'"
input "SECRET_ACTIVITI_JDBC_PASSWORD" "idmuser" "Set OpenIAM password for schema 'activiti'. For MySQL it will be the same as for 'openiam'"
input "SECRET_RABBITMQ_PASSWORD" "passwd00" "Set OpenIAM password for RabbitMQ message broker"
input "SECRET_REDIS_PASSWORD" "passwd00" "Set OpenIAM password for Redis."
input "SECRET_MAIL_USERNAME" "none" "Set SMTP username. You can change it later."
input "SECRET_MAIL_PASSWORD" "none" "Set SMTP password. You can change it later."

: "${SECRET_IAM_JKS_PASSWORD:?SECRET_IAM_JKS_PASSWORD is required}"
: "${SECRET_IAM_JKS_KEY_PASSWORD:?SECRET_IAM_JKS_KEY_PASSWORD is required}"
: "${SECRET_IAM_JKS_COOKIE_KEY_PASSWORD:?SECRET_IAM_JKS_COOKIE_KEY_PASSWORD is required}"
: "${SECRET_IAM_JKS_COMMON_KEY_PASSWORD:?SECRET_IAM_JKS_COMMON_KEY_PASSWORD is required}"
: "${SECRET_KEYSTORE_PASSWORD:?SECRET_KEYSTORE_PASSWORD is required}"

: "${SECRET_JDBC_USERNAME:?SECRET_JDBC_USERNAME is required}"
: "${SECRET_JDBC_PASSWORD:?SECRET_JDBC_PASSWORD is required}"

: "${SECRET_ACTIVITI_JDBC_USERNAME:?SECRET_ACTIVITI_JDBC_USERNAME is required}"
: "${SECRET_ACTIVITI_JDBC_PASSWORD:?SECRET_ACTIVITI_JDBC_PASSWORD is required}"

: "${SECRET_RABBITMQ_PASSWORD:?SECRET_RABBITMQ_PASSWORD is required}"
: "${SECRET_REDIS_PASSWORD:?SECRET_REDIS_PASSWORD is required}"

: "${SECRET_MAIL_USERNAME:?SECRET_MAIL_USERNAME is required}"
: "${SECRET_MAIL_PASSWORD:?SECRET_MAIL_PASSWORD is required}"

export SECRET_SPRING_REDIS_PASSWORD=$SECRET_REDIS_PASSWORD
export SECRET_SPRING_RABBITMQ_PASSWORD=$SECRET_RABBITMQ_PASSWORD


token=$(curl -k --request POST --cert ${VAULT_CERTS}/vault.crt --key ${VAULT_CERTS}/vault.key --data '{"name": "web"}' https://${VAULT_URL}:${VAULT_PORT}/v1/auth/cert/login | jq .auth.client_token  | tr -d '"')

if [ -z "$token" ]; then
	echo "Token was empty.  Can't initialize vault"
	exit 1;
fi

for secret in `env | grep SECRET_ | cut -f1 -d"="`; do
	key=`echo ${secret:7}`
	modified_key=`echo $key | tr "_" "."n  | awk '{print tolower($0)}'`
	value=`printf '%s\n' "${!secret}"`


	value=`echo $value | sed 's/"//g'`
	value=`echo $value | sed "s/'//g"`

	curl -k \
    --header "X-Vault-Token: $token" \
    --request POST \
    --cert ${VAULT_CERTS}/vault.crt --key ${VAULT_CERTS}/vault.key \
    --data '{"value": "'$value'"}' \
    https://${VAULT_URL}:${VAULT_PORT}/v1/secret/openiam/$key

	curl -k \
    --header "X-Vault-Token: $token" \
    --request POST \
    --cert ${VAULT_CERTS}/vault.crt --key ${VAULT_CERTS}/vault.key \
    --data '{"value": "'$value'"}' \
    https://${VAULT_URL}:${VAULT_PORT}/v1/secret/openiam/vault.secret.$modified_key
done

chown vault:vault /usr/local/openiam/vault/certs/unseal.keys
