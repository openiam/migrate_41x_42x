#!/bin/bash

set -e
#### THIS IS ELASTICSEARCH INITIALIZATION SCRIPT
. /usr/local/openiam/env.conf
export VAULT_CERTS="$HOME_DIR/vault/certs/"
export JAVA_HOME="$HOME_DIR/jdk"
export VAULT_HOME="$HOME_DIR/utils/vault/"

#elasticsearch
rpm -U --nosignature $HOME_DIR/dependencies/elasticsearch-6.8.7.rpm

cp -rf /usr/share/elasticsearch/bin/elasticsearch /usr/share/elasticsearch/bin/elasticsearch.orig
cp -rf $HOME_DIR/utils/elasticsearch/elasticsearch /usr/share/elasticsearch/bin/elasticsearch

cp -rf /etc/elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml.orig
cp -rf $HOME_DIR/utils/elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml

systemctl enable elasticsearch
systemctl start elasticsearch
echo "Starting elasticsearch..."
sleep 10
