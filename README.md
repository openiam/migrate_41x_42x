# OpenIAM Migration from 4.1.X to 4.2.0.X tool
This document describes how to update existed OpenIAM instance from version 4.1.X to version 4.2.0.X
You can find several scripts in this repository which help you in the migration process.

## Preparation

Clone this repository on the server where it is required to update OpenIAM. git should be installed before.
You might use the command:

```
 yum install git -y
 git clone https://bitbucket.org/openiam/migrate_41x_42x.git
```

Please download the latest OpenIAM 4.2.0.X RPM distributive file openiam-4.2.0.X-1.el7.x86_64.rpm
and put it in the folder migrate_41x_42x (cloned from the git).

Perform the 1st step of the migration:

```
./steps/updating_1.sh
```

If all is set, and you know database schema names and db user's credentials you CAN run

```
openiam-cli init
```

Follow default OpenIAM installation procedure.
