#!/bin/bash

# check connection to DB in case it's local
function check_connection() {
    mysqladmin -uroot -p$1 version > /dev/null 2>/dev/null
}
#
function prompt_local_repo() {
  read -p "Do you need to install a local yum repository with OpenIAM's required dependencies? We recommend to asnwer 'y' only in case you don't have access to central yum repository [y/n]:" is_repo
  case "$is_repo" in
    [yY][eE][sS]|[yY])
        echo "Installing local repository to /usr/local/openiam-repository/"
        mkdir -p /usr/local/openiam-repository/
        cp -rf ./dependencies/yum.repos.d/OpenIAM-Base-Local.repo /etc/yum.repos.d/OpenIAM-Base-Local.repo
        cp -rf ./dependencies/common/* /usr/local/openiam-repository/
        yum update -y
        ;;
    [nN][oO]|[nN])
        echo "Use central repository."
        ;;
    *)
      	echo "Please answer [y/n]"
        prompt_db
        ;;
  esac
}


function prompt_db() {
  read -p "We detected local MariaDB Server. Do you use it for OpenIAM? If so please answer y. We will update it automatically. You MUST know root password. Do you want to update MariaDB? [y/n]:" is_mariadb
  case "$is_mariadb" in
    [yY][eE][sS]|[yY])
        install_db
        ;;
    [nN][oO]|[nN])
        echo "Skip MariaDB update"
        ;;
    *)
      	echo "Please answer [y/n]"
        prompt_db
        ;;
  esac
}

function install_db(){
        echo -n "MariadB root password (Enter if root password is not set):"
        read -s root_password
        echo
	      check_connection $root_password
        local status=$?
        if [ $status -ne 0 ];
        then
          echo "Root password is wrong"
          prompt_db
        else
          echo "Applying Update"
          echo "MariaDB Server"
          systemctl stop mariadb
          cp -rf ./dependencies/yum.repos.d/MariaDB-Base.repo /etc/yum.repos.d/MariaDB-Base.repo
          echo "Force update MariaDB Server"
          yum update mariadb -y
          systemctl enable --now mariadb
          mysql_upgrade -uroot -p"${root_password}"
        fi
}
prompt_local_repo

echo "Installing some required packages"

yum install unzip zip gcc -y

echo "Stopping and disabling OpenIAM service"
systemctl stop elasticsearch
systemctl stop openiam-esb
systemctl stop openiam-groovy
systemctl stop openiam-idm
systemctl stop openiam-reconciliation
systemctl stop openiam-redis
systemctl stop openiam-synchronization
systemctl stop openiam-ui
systemctl stop openiam-workflow
systemctl stop rabbitmq-server
systemctl stop httpd

service openiam stop
service openiam-ui stop
service openiam-redis stop

systemctl disable elasticsearch
systemctl disable openiam-esb
systemctl disable openiam-groovy
systemctl disable openiam-idm
systemctl disable openiam-reconciliation
systemctl disable openiam-redis
systemctl disable openiam-synchronization
systemctl disable openiam-ui
systemctl disable openiam-workflow
systemctl disable rabbitmq-server

echo "Installing new OpenIAM"
rpm_files=$(ls -al openiam-4.2.*.rpm | awk '{ print $9 }' | sort -r)
latest_rpm_file=$(echo "$rpm_files" | cut -d ' ' -f1)
rpm -i "$latest_rpm_file"

echo "Copying secrets to a new location"
mkdir -p /usr/local/openiam/conf/jks
cp -rf /usr/local/OpenIAM/data/openiam/conf/jks/openiam.jks /usr/local/openiam/conf/jks/openiam.jks
chown -R openiam:openiam /usr/local/openiam/conf/jks

echo "Copying OLD groovy scripts to the new groovy scripts folder"
mkdir -p /usr/local/openiam/conf/iamscripts/BACKUPED
cp -rf /usr/local/OpenIAM/data/openiam/conf/iamscripts/* /usr/local/openiam/conf/iamscripts/BACKUPED/
chown -R openiam:openiam /usr/local/openiam/conf/iamscripts/BACKUPED/



echo "Copying migration scripts"
echo "Initialization script"
cp -rf /usr/local/openiam/utils/init.sh /usr/local/openiam/utils/init.sh.newinst
cp -rf ./dependencies/init.sh /usr/local/openiam/utils/init.sh
chmod +x /usr/local/openiam/utils/init.sh

echo "RabbitMQ"
cp -rf /usr/local/openiam/utils/rabbitmq/init.sh /usr/local/openiam/utils/rabbitmq/init.sh.newinst
cp -rf ./dependencies/rabbitmq/init.sh /usr/local/openiam/utils/rabbitmq/init.sh
chmod +x /usr/local/openiam/utils/rabbitmq/init.sh

echo "ElasticSearch"
cp -rf /usr/local/openiam/utils/elasticsearch/init.sh /usr/local/openiam/utils/elasticsearch/init.sh.newinst
cp -rf ./dependencies/elasticsearch/init.sh /usr/local/openiam/utils/elasticsearch/init.sh
chmod +x /usr/local/openiam/utils/elasticsearch/init.sh

echo "Vault"
cp -rf /usr/local/openiam/utils/vault/bootstrap.sh /usr/local/openiam/utils/vault/bootstrap.sh.newinst
cp -rf ./dependencies/vault/bootstrap.sh /usr/local/openiam/utils/vault/bootstrap.sh
chmod +x /usr/local/openiam/utils/vault/bootstrap.sh

echo "Flyway"
cp -rf /usr/local/openiam/utils/flyway/init.sh /usr/local/openiam/utils/flyway/init.sh.newinst
cp -rf ./dependencies/flyway/init.sh /usr/local/openiam/utils/flyway/init.sh
chmod +x /usr/local/openiam/utils/flyway/init.sh

echo "Fixinig migration scripts compatibility"
cp -rf ./dependencies/scripts/mssql/openiam/4.2.0.0/V4.2.0.0.021__IAM-3609.sql /usr/local/openiam/conf/schema/mssql/openiam/4.2.0.0/V4.2.0.0.021__AM-3609.sql
cp -rf ./dependencies/scripts/mysql/openiam/4.2.0.0/V4.2.0.0.021__IAM-3609.sql /usr/local/openiam/conf/schema/mysql/openiam/4.2.0.0/V4.2.0.0.021__IAM-3609.sql
cp -rf ./dependencies/scripts/oracle/openiam/4.2.0.0/V4.2.0.0.021__IAM-3609.sql /usr/local/openiam/conf/schema/oracle/openiam/4.2.0.0/V4.2.0.0.021__IAM-3609.sql
cp -rf ./dependencies/scripts/postgres/openiam/4.2.0.0/V4.2.0.0.021__IAM-3609.sql /usr/local/openiam/conf/schema/postgres/openiam/4.2.0.0/V4.2.0.0.021__IAM-3609.sql




echo "Checking local RDBMS"
yum list | grep -i mariadb-server 1>/dev/null
if [ $? -eq 0 ]; then
  prompt_db
else
  echo "MariaDB Service is not installed on this server."
  echo "YOU MUST UPDATE YOU DATABASE SERVER MANUALLY."
  echo "IN CASE YOU USE MARIADB THE VERSION MUST BE AT LEAST 10.5.X"
fi

echo "Cleaning up old services"
rm -rf /etc/init.d/openiam*
systemctl daemon-reload

pkill redis

echo "The system is ready for migration"
echo "Now is the best time to update MariaDB Server if is has not been done before automatically or manually"
echo "For the next step you will need to know the schema names and db user's credentials"
echo "Redis/RabbitMQ passwords might be different"
echo "**********************************************************"
echo "** Run [openiam-cli init] command to continue migration **"
echo "**********************************************************"
